package org.openjfx;

import javafx.application.Application;

public class MainApp {

  public static void main(String[] args) {
    Application.launch(App.class, args);
  }

}
